# frozen_string_literal: true

require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations

    def biased_mean(grades, blacklist)
      all_students = JSON.parse(grades)
      array_of_denied_users = blacklist.split

      filtered_students = all_students.reject { |key, _value| array_of_denied_users.include?(key) }
      filtered_students.values.sum / filtered_students.size
    end

    def no_integers(numbers)
      rule = %w[00 25 50 75]
      array_of_numbers = numbers.split
      array_of_numbers.map { |x| rule.include?(x[-2..]) ? 'S' : 'N' }.join(' ')
    end

    def filter_films(genres = 'Fantasy War', year = 1990)
      films = get_films
      array_of_genres = genres.split
      films[:movies]
        .select { |film| film[:year].to_i >= year }
        .select { |film| array_of_genres & film[:genres] == array_of_genres }
        .map { |film| film[:title] }
        .join("\n")
    end

    private

    def get_films
      JSON.parse(File.read('db.json'), symbolize_names: true)
    end

    def refresh_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      result = JSON.parse(Net::HTTP.get(URI(url)), symbolize_names: true)
      File.write('db.json', JSON.dump(result))
      result
    end
  end
end
